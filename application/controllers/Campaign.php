<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends CI_Controller {

    public function index()
    {
        $this->load->view('campaign_view');
    }

    public function volunteer()
    {
        $this->load->view('volunteer_view');
    }

    public function contact()
    {
        $this->load->view('contact_view');
    }

}