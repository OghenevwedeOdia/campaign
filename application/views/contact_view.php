<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="<?php echo base_url(); ?>img/fav.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Seyi J Adisa Campaign</title>

        <!-- Icon css link -->
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/elegant-icon/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/themify-icon/themify-icons.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="<?php echo base_url(); ?>vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/animate-css/animate.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="<?php echo base_url(); ?>vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/new_style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!--================Search Area =================-->
        <section class="search_area">
            <div class="search_inner">
                <input type="text" placeholder="Enter Your Search...">
                <i class="ti-close"></i>
            </div>
        </section>
        <!--================End Search Area =================-->

        <!--================Header Menu Area =================-->
        <header class="main_menu_area">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#"><img src="<?php echo base_url(); ?>img/logo1.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                        <li class="nav-item "><a class="nav-link" href="<?php echo base_url() ;?>">HOME</a></li>
                        <li class="nav-item active"><a class="nav-link" href="<?php echo site_url('contact');?>">MEET-SEYI</a></li>
                        <li class="nav-item"><a  href="#">ISSUES</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">GET INVOLVED</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">GALLERY</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('volunteer');?>">VOLUNTEER</a></li>
                    </ul>
                    <ul class="navbar-nav justify-content-end">
                        <li><a href="#"><i class="icon_search"></i></a></li>
                        <!-- <i class="icon_bag_alt"></i> -->
                        <li><a href="#">DONATE</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--================End Header Menu Area =================-->

        <!--================Banner Area =================-->
        <section class="banner_area">
            <div class="container">
                <div class="banner_text_inner">
                    <!-- <h4>Contact Us</h4>
                    <h5>Tell us about your story and your project.</h5> -->
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->

        <!--================Contact Us Area =================-->
        <section class="contact_us_area">
            <div class="container">
                <!-- <div id="mapBox" class="mapBox row m0"
                    data-lat="1.30921"
                    data-lon="103.8509813"
                    data-zoom="14"
                    data-marker="img/map-marker.png"
                    data-info="Broadway Hotel"
                    data-mlat="1.30921"
                    data-mlon="103.8509813">
                </div> -->
                <div class="contact_details_inner">
                    <div class="row">
                    <div class="col-md-7">
                            <div class="contact_form">
                            <p>
                           <?php echo nl2br("Oluseyi Joseph Adisa was born in 1983 and is an indigene of Afijo in Oyo State. He studied Law at the University of Birmingham and was called to bar in Nigeria in the year 2010. He is an associate member of the institute of chartered secretaries and administrators in Nigeria and has continued to improve himself by attending training programmes in respected institutions such as Harvard while working on his Masters in public administration at the University of Birmingham.

Seyi is currently the principal private secretary to the executive governor of Oyo State, a capacity in which he has acted over the last 7 years. He manages the day-to-day programmes of the governor including the governor’s itinerary, meetings, correspondence and reports while also advising the Governor closely on processes, policies and their possible implications. He also conveys instructions and requests for information from His Excellency to the relevant offices in the State Government and ensures that appropriate follow up action is taken. Seyi is adept in planning, administration, organization and has excellent interpersonal skills; all these are necessary traits he has had to acquire to efficiently serve in this role.

Seyi is a respected Administrator and Lawyer. Prior to taking up the call to public service, he founded Tunde &amp; Adisa Legal Practitioners (T&A Legal), a position that he has held since February 2010. In this role, he actively formulates strategies to align the Firm to its set vision and objectives, manages the creation of systems, financial planning, staffing, risk management, information technology, training and marketing. Seyi understands the importance of entrepreneurship in tackling the ever-increasing levels of unemployment in Nigeria, so he has actively contributed to the growth of young entrepreneurs by offering pro bono legal services to start ups.

An Imaginative thinker serial innovator and lover of sports, Seyi Founded Seven11Continental Limited in August 2007, a startup focused on creating the first fantasy football game in Nigeria with users from all over the world. His ability to recognize when to take calculated risks and the confidence to make tough business decisions were pivotal to the running of this startup for 3 years. Seyi has also mentored and coached football to young kids in the United Kingdom.

As a believer in giving back to the community, Seyi has served as an impact director in Global Harvest Church Bodija, Ibadan for more than 6 years. He continues to volunteer in this position, coordinating people and resources to cater for the less privileged in the society through numerous outreach programmes, including free medical services, feeding and clothing schemes etc.

Seyi understands that Nigeria is nothing without its people and believes that we can achieve a lot more as a country if we directly invest in the people. With this understanding, Seyi plans to increase access to healthcare (especially in rural communities) create jobs and support businesses, improve the quality of grassroots education and build necessary infrastructure to improve the quality of life of the people.
                            ")?></p>
                                <!-- <div class="main_title">
                                    <h2>Get In Touch With Us!</h2>
                                    <p>Fill out the form below to recieve a free and confidential.</p>
                                </div>
                                <form class="contact_us_form row" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                                    <div class="form-group col-lg-12">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Website">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <textarea class="form-control" name="message" id="message" rows="1" placeholder="Message"></textarea>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <button type="submit" value="submit" class="btn submit_btn2 form-control">Send Messages</button>
                                    </div>
                                </form> -->
                            </div>
                        </div>
                        <div class="col-md-5">
                        <img src="<?php echo base_url(); ?>img/about_seyi.jpg" style="width:100%;" class="img-thumbnail"  alt=""data-bgposition="left left"    data-bgrepeat="no-repeat"  data-no-retina>

                            <!-- <div class="contact_text">
                                <div class="main_title">
                                    <h2>Contact Us</h2>
                                    <p>There are many ways to contact us. You may drop us a line, give us a call or send an email, choose what suits you the most</p>
                                </div>
                                <div class="contact_d_list">
                                    <div class="contact_d_list_item">
                                        <a href="#">(800) 686-6688</a>
                                        <a href="#">info.deercreative@gmail.com</a>
                                    </div>
                                    <div class="contact_d_list_item">
                                        <p>40 Baria Sreet 133/2 <br /> NewYork City, US</p>
                                    </div>
                                    <div class="contact_d_list_item">
                                        <p>Open hours: 8.00-18.00 Mon-Fri <br />Sunday: Closed</p>
                                    </div>
                                </div>
                                <div class="static_social">
                                    <div class="main_title">
                                        <h2>Follow Us:</h2>
                                    </div>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div> -->
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!--================End Contact Us Area =================-->

        <!--================Footer Area =================-->
        <footer class="footer_area">
            <div class="footer_widgets_area">
                <div class="container">
                    <div class="f_widgets_inner row">
                        <!-- <div class="col-lg-3 col-md-6">
                            <aside class="f_widget subscribe_widget">
                                <div class="f_w_title">
                                    <h3>Our Newsletter</h3>
                                </div>
                                <p>Subscribe to our mailing list to get the updates to your email inbox.</p>
                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="E-mail" aria-label="E-mail">
                                    <span class="input-group-btn">
                                        <button class="btn btn-secondary submit_btn" type="button">Subscribe</button>
                                    </span>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </aside>
                        </div> -->
                        
                        <div class=" col-md-5">
                            <aside class="f_widget categories_widget">
                                <div class="f_w_title">
                                    <h3>Link Categories</h3>
                                </div>
                                <ul>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>HOME</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>METT-SEYI</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>ISSUES</a></li>
                                        </div>
                                        <div class="col-md-6">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>GETINVOLVED</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>GALLERY</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>DONATE</a></li>
        
                                </div>
                                </div>
                                    
                                    
                                </ul>
                                
                                <!-- <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Home</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>About</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Services</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Work</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Privacy</a></li>
                                </ul> -->
                            </aside>
                        </div>
                        <div class="col-md-3">
                        <img src="<?php echo base_url(); ?>img/footer_image.png" class="footer_image"  alt=""/>
                        </div>
                        <div class=" col-md-4">
                        <aside class="f_widget contact_widget">
                                <div class="f_w_title">
                                    <h3>Contact Us</h3>
                                </div>
                                <a href="#">07034367931</a>
                                <a href="#">info.techbarn@gmail.com</a>
                                <p>35 Olorun Oluwa Street <br />Ikoyi Lagos</p>
                                <h6>Open hours: 8.00-18.00 Mon-Fri</h6>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy_right_area">
                <div class="container">
                    <div class="float-md-left">
                        <h5>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://techbarn.com" target="_blank">TechBarn</a></h5>
                    </div>
                    <div class="float-md-right">
                        <ul class="nav">
                            <!-- <li class="nav-item">
                                <a class="nav-link active" href="#">Disclaimer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Privacy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Advertisement</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact us</a>
                            </li> -->
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!--================End Footer Area =================-->

        <!--================Contact Success and Error message Area =================-->
        <div id="success" class="modal modal-message fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-close"></i>
                        </button>
                        <h2>Thank you</h2>
                        <p>Your message is successfully sent...</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modals error -->

        <div id="error" class="modal modal-message fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-close"></i>
                        </button>
                        <h2>Sorry !</h2>
                        <p> Something went wrong </p>
                    </div>
                </div>
            </div>
        </div>
        <!--================End Contact Success and Error message Area =================-->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="<?php echo base_url(); ?>vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="<?php echo base_url(); ?>vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/jquery.counterup.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/apear.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/countto.js"></script>
        <script src=<?php echo base_url(); ?>"vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/parallaxer/jquery.parallax-1.1.3.js"></script>
        <!--Tweets-->
        <script src="<?php echo base_url(); ?>vendors/tweet/tweetie.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/tweet/script.js"></script>
        <!--gmaps Js-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="<?php echo base_url(); ?>js/gmaps.min.js"></script>

        <!-- contact js -->
        <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>js/contact.js"></script>

        <script src="<?php echo base_url(); ?>js/theme.js"></script>
    </body>
</html>
