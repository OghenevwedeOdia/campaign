<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="<?php echo base_url();?>img/fav.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Seyi J Adisa Campaign</title>

        <!-- Icon css link -->
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/elegant-icon/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/themify-icon/themify-icons.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="<?php echo base_url(); ?>vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/animate-css/animate.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="<?php echo base_url(); ?>vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/new_style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!--================Search Area =================-->
        <section class="search_area">
            <div class="search_inner">
                <input type="text" placeholder="Enter Your Search...">
                <i class="ti-close"></i>
            </div>
        </section>
        <!--================End Search Area =================-->

        <!--================Header Menu Area =================-->
        <header class="main_menu_area">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#"><img src="<?php echo base_url();?>img/logo1.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url() ;?>">HOME</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('contact');?>">MEET-SEYI</a></li>
                        <li class="nav-item"><a  href="#">ISSUES</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">GET INVOLVED</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">GALLERY</a></li>
                        <li class="nav-item active"><a class="nav-link" href="<?php echo site_url('volunteer');?>">VOLUNTEER</a></li>
                    </ul>
                    <ul class="navbar-nav justify-content-end">
                        <li><a href="#"><i class="icon_search"></i></a></li>
                        <!-- <i class="icon_bag_alt"></i> -->
                        <li><a href="#">DONATE</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--================End Header Menu Area =================-->

        <!--================Banner Area =================-->
        <section class="banner_area">
            <div class="container">
                <div class="banner_text_inner">
                    <u style="color:#2bc0a4; font-size:90px;"><h1>Volunteer</h1></u>
                   
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->

        <!--================Contact Us Area =================-->
        <section class="contact_us_area">
            <div class="container"><br>
                <div class="row breadcrumb">
                    <div class="col-md-8">
                        <p>VOLUNTEER</p>
                    </div>
                    <div class="col-md-4">
                                <ul class="form-inline">
                                    <li class="nav-item active"><a href="<?php echo site_url('volunteer');?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Volunteer </a></li>
                                 <li><a href="<?php echo base_url() ;?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Home</a></li>
                                 </ul>
                    </div>
                </div>
                <!-- <div id="mapBox" class="mapBox row m0"
                    data-lat="1.30921"
                    data-lon="103.8509813"
                    data-zoom="14"
                    data-marker="img/map-marker.png"
                    data-info="Broadway Hotel"
                    data-mlat="1.30921"
                    data-mlon="103.8509813">
                </div> -->
                <div class="contact_details_inner">
                    <div class="row">
                       
                        <div class="col-lg-12">
                            <div class="contact_form">
                                <div class="main_title">
                                    <h2>Get In Touch With Us!</h2>
                                    <p>Fill out the form below to we'll get back to you</p>
                                </div>
                                <form class="contact_us_form row" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                                    <div class="form-group col-lg-12">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Input Email">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <input type="number" class="form-control" id="subject" name="subject" placeholder="Input Telephone Number">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <textarea class="form-control" name="message" id="message" rows="3" placeholder="How would you like to volunteer?"></textarea>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <button type="submit" value="submit" class="btn submit_btn2 form-control">Send</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Contact Us Area =================-->

<!--================Footer Area =================-->
<footer class="footer_area">
            <div class="footer_widgets_area">
                <div class="container">
                    <div class="f_widgets_inner row">
                        <!-- <div class="col-lg-3 col-md-6">
                            <aside class="f_widget subscribe_widget">
                                <div class="f_w_title">
                                    <h3>Our Newsletter</h3>
                                </div>
                                <p>Subscribe to our mailing list to get the updates to your email inbox.</p>
                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="E-mail" aria-label="E-mail">
                                    <span class="input-group-btn">
                                        <button class="btn btn-secondary submit_btn" type="button">Subscribe</button>
                                    </span>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </aside>
                        </div> -->
                        
                        <div class=" col-md-5">
                            <aside class="f_widget categories_widget">
                                <div class="f_w_title">
                                    <h3>Link Categories</h3>
                                </div>
                                <ul>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>HOME</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>METT-SEYI</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>ISSUES</a></li>
                                        </div>
                                        <div class="col-md-6">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>GETINVOLVED</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>GALLERY</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>DONATE</a></li>
        
                                </div>
                                </div>
                                    
                                    
                                </ul>
                                
                                <!-- <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Home</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>About</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Services</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Work</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Privacy</a></li>
                                </ul> -->
                            </aside>
                        </div>
                        <div class="col-md-3">
                        <img src="<?php echo base_url(); ?>img/footer_image.png" class="footer_image" style="width=20%;" alt=""/>
                        </div>
                        <div class=" col-md-4">
                        <aside class="f_widget contact_widget">
                                <div class="f_w_title">
                                    <h3>Contact Us</h3>
                                </div>
                                <a href="#">07034367931</a>
                                <a href="#">info.techbarn@gmail.com</a>
                                <p>35 Olorun Oluwa Street <br />Ikoyi Lagos</p>
                                <h6>Open hours: 8.00-18.00 Mon-Fri</h6>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy_right_area">
                <div class="container">
                    <div class="float-md-left">
                        <h5>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://techbarn.com" target="_blank">TechBarn</a></h5>
                    </div>
                    <div class="float-md-right">
                        <ul class="nav">
                            <!-- <li class="nav-item">
                                <a class="nav-link active" href="#">Disclaimer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Privacy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Advertisement</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact us</a>
                            </li> -->
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!--================End Footer Area =================-->


        <!--================Contact Success and Error message Area =================-->
        <div id="success" class="modal modal-message fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-close"></i>
                        </button>
                        <h2>Thank you</h2>
                        <p>Your message is successfully sent...</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modals error -->

        <div id="error" class="modal modal-message fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-close"></i>
                        </button>
                        <h2>Sorry !</h2>
                        <p> Something went wrong </p>
                    </div>
                </div>
            </div>
        </div>
        <!--================End Contact Success and Error message Area =================-->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="<?php echo base_url(); ?>vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="<?php echo base_url(); ?>vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/jquery.counterup.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/apear.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/countto.js"></script>
        <script src="<?php echo base_url(); ?>vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/parallaxer/jquery.parallax-1.1.3.js"></script>
        <!--Tweets-->
        <script src="<?php echo base_url(); ?>vendors/tweet/tweetie.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/tweet/script.js"></script>
        <!--gmaps Js-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="<?php echo base_url(); ?>js/gmaps.min.js"></script>

        <!-- contact js -->
        <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>js/contact.js"></script>

        <script src="<?php echo base_url(); ?>js/theme.js"></script>
    </body>
</html>
