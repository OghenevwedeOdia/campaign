<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="stylesheet" href="<?php //echo base_url(); ?>css/mdb.min.css" /> -->

        <link rel="icon" href="<?php echo base_url(); ?>img/fav.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Seyi J Adisa Campaign</title>

        <!-- Icon css link -->
        
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>vendors/elegant-icon/style.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>vendors/themify-icon/themify-icons.css" rel="stylesheet"/>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet"/>

        <!-- Rev slider css -->
        <link href="<?php echo base_url(); ?>vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendors/animate-css/animate.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="<?php echo base_url(); ?>vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/new_style.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!--================Search Area =================-->
        <section class="search_area">
            <div class="search_inner">
                <input type="text" placeholder="Enter Your Search...">
                <i class="ti-close"></i>
            </div>
        </section>
        <!--================End Search Area =================-->

        <!--================Header Menu Area =================-->
        <header class="main_menu_area">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#"><img src="<?php echo base_url(); ?>img/logo1.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="<?php echo base_url() ;?>">HOME</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('contact');?>">MEET-SEYI</a></li>
                        <li class="nav-item"><a  href="#">ISSUES</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">GET INVOLVED</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">GALLERY</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('volunteer');?>">VOLUNTEER</a></li>
                    </ul>
                    <ul class="navbar-nav justify-content-end">
                        <li><a href="#"><i class="icon_search"></i></a></li>
                        <!-- <i class="icon_bag_alt"></i> -->
                        <li><a href="#">DONATE</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--================End Header Menu Area =================-->

        <!--================Slider Area =================-->
        <section class="main_slider_area">
            <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
                <ul> 
                    <li data-index="rs-2946" data-transition="slidevertical" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="2" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url(); ?>img/banner1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme secand_text"
                                data-x="['left','left','left','left','left','left']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['center','center','center','center','center','center']"
                                data-voffset="['0','0','0','0','0']"
                                data-fontsize="['48','48','48','28','28','22']"
                                data-lineheight="['60','60','60','36','36','30']"
                                data-width="100%"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text"
                                data-responsive_offset="on"
                                data-transform="o:1;"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                data-textAlign="['left','left','left','left','left','left']"

                                style="z-index: 8;font-family:'Poppins', sans-serif;font-weight:700;color:#fff;">E jé ká Sèyi<br /> <br/> <br/> 
                            </div>

                            <div class="tp-caption tp-resizeme secand_text"
                                data-x="['left','left','left','left','left','left']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['center','center','center','center','center','center']"
                                data-voffset="['0','0','0','0','0']"
                                data-fontsize="['20','20','20','20','20','20']"
                                data-lineheight="['60','60','60','36','36','30']"
                                data-width="100%"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text"
                                data-responsive_offset="on"
                                data-transform_idle="o:1;"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                data-textAlign="['left','left','left','left','left','left']"

                                style="z-index: 8;font-family:'Poppins', sans-serif;font-weight:700;color:#fff;"><br> Fun itesiwaju <br> #Afijio2019
                            </div>
                          
                        </li>
                </ul>
            </div>
        </section>
        <!--================End Slider Area =================-->

        <!--================Creative Feature Area =================-->
        <section class="creative_feature_area">
            <div class="container">
                <div class="c_feature_box">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="c_box_item">
                                <a href="#"><h4><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Get Out and Vote !</h4></a>
                                <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.

Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring. </p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="c_box_item">
                                <a href="#"><h4><i class="fa fa-clock-o" aria-hidden="true"></i>Today We Change Everything!</h4></a>
                            <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.

Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <!--================Latest News Area =================-->
        <section class="latest_news_area p_100">
            <div class="">
                <!-- <div class="b_center_title">
                    <h2>Latest News</h2>
                    <p>We Are A Creative Digital Agency. Focused on Growing Brands Online</p>
                </div> -->
                <div class="l_news_inner">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url(); ?>img/volunteer.png" alt=""></a></div>
                                
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url(); ?>img/events.png" alt=""></a></div>
                               
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url(); ?>img/donations.png" alt=""></a></div>
                               
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url(); ?>img/elections.png" alt=""></a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url(); ?>img/mission.png" alt=""></a>
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url(); ?>img/issues.png" alt=""></a>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!--================End Latest News Area =================-->
                <div class="digital_feature p_100">
                <div class="b_center_title">
                    <h2>Lets Do This – E jé ká Sèyí!</h2>
                </div>
                    <div class="row">
                    <div class="col-lg-6">
                            <div class="d_feature_img">
                                <img src="img/seyi-adisa.jpg" alt="">
                            </div>
                        </div>   
                    <div class="col-lg-6">
                        <div class="c_feature_box_one">
                            <div class="d_feature_text">
                                <div class="c_box_item">
                            <a href="#"><h4><i class="fa fa-clock-o" aria-hidden="true"></i>Be With Us From Now!</h4></a>
                                <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.

Capitalise on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                                
                                </div>
                                
                        </div>
                            </div>

                            <div class="c_feature_box_one">
                            <div class="d_feature_text">
                                <div class="c_box_item">
                            <a href="#">
                                <h4><i class="fa fa-angle-double-down" aria-hidden="true"></i>Join Our Next Events</h4></a>
                                <p>
                                Capitalise on low hanging fruit to identify a 
                                ballpark value added activity to beta test. Override
                                the digital divide with additional clickthroughs from
                                 DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.
                                </p>
                                </div>
                                
                        </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!--================End Creative Feature Area =================-->

        <!--================Our Service Area =================-->
        <section class="service_area">
            <!-- <div class="container">
                <div class="center_title">
                    <h2>Join Our Mailing List</h2>
                    <p>Our Team Needs You</p>
                </div>
                <div class="row animate-box">
					<center>
						<form class="form-inline">
							<div class="col-md-6 ">
								
								<label for="name" class="sr-only ">Name</label>
								<input type="name" class="form-control" id="name" placeholder="Name">
								
							</div>
							
							<div class="col-md-6 ">
                                <button type="submit" class="btn btn-primary btn-email">JOIN US</button>
                               
							</div>
						</form>
                        </center>
				</div>
            </div> -->
            <div class="container">
                <div class="project_inner">
                    <div class="center_title">
                        <h2>Join Our Mailing List</h2>
                        <p>Our Team Needs You</p>
                    </div>
                    <div class="row ">
                    <div class="col-md-6">
                     <input type="name" class="form-control home_mailing" id="email" placeholder="Email"><br>
                    </div>
                    <div class="col-md-6">
                    <input type="name" class="form-control home_mailing" id="name" placeholder="Name"><br>
                    
                    </div>
                    <a class="tp_btn" href="#">JOIN US</a>
                    </div>
                   
                    
                   
                </div>
            </div>
        </section>
        <!--================End Our Service Area =================-->

        <!--================Testimonials Area =================-->
        <section class="testimonials_area p_100">
            <div class="container">
                <div class="testimonials_slider owl-carousel">
                    <div class="item">
                        <div class="media">
                            <img class="d-flex rounded-circle" src="img/abiola-ajimobi-ejekaseyi.png" alt="">
                            <div class="media-body">
                                <img src="<?php echo base_url(); ?>img/dotted-icon.png" alt="">
                                <p>Every step toward the goal of justice requires sacrifice, suffering, and struggle; the tireless exertions and passionate concern</p>
                                <strong>HE Sen. Abiola Ajimobi</strong>
                                <h5>Governor of Oyo State <span>-</span> HE Sen. Abiola Ajimobi</h5>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="media">
                            <img class="d-flex rounded-circle" src="img/aalafin-of-oyo-ejekaseyi.png" alt="">
                            <div class="media-body">
                                <img src="<?php echo base_url(); ?>img/dotted-icon.png" alt="">
                                <p>You have young men of color in many communities in Oyo. And, you know, part of my job,</p>
                               <strong>Aalafin of Oyo</strong>
                                <h5>Aalafin of Oyo <span>-</span> Dr. Lamidi Adeyemi III</h5>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!--================End Testimonials Area =================-->

        <!--================Project Area =================-->
        <section class="project_area">
            <div class="container">
                <div class="project_inner">
                    <div class="center_title">
                        <h2>Ready To Volunteer? </h2>
                        <p>There are many ways you can volunteer. You may drop us a line, give us a call or send an email, choose what suits you the most.</p>
                    </div>
                    <a class="tp_btn" href="<?php echo site_url('volunteer');?>">VOLUNTEER</a>
                </div>
            </div>
        </section>
        <!--================End Project Area =================-->

        

        <!--================Footer Area =================-->
        <footer class="footer_area">
            <div class="footer_widgets_area">
                <div class="container">
                    <div class="f_widgets_inner row">
                        <!-- <div class="col-lg-3 col-md-6">
                            <aside class="f_widget subscribe_widget">
                                <div class="f_w_title">
                                    <h3>Our Newsletter</h3>
                                </div>
                                <p>Subscribe to our mailing list to get the updates to your email inbox.</p>
                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="E-mail" aria-label="E-mail">
                                    <span class="input-group-btn">
                                        <button class="btn btn-secondary submit_btn" type="button">Subscribe</button>
                                    </span>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </aside>
                        </div> -->
                        
                        <div class=" col-md-5">
                            <aside class="f_widget categories_widget">
                                <div class="f_w_title">
                                    <h3>Link Categories</h3>
                                </div>
                                <ul>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>HOME</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>METT-SEYI</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>ISSUES</a></li>
                                        </div>
                                        <div class="col-md-6">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>GETINVOLVED</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>GALLERY</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>DONATE</a></li>
        
                                </div>
                                </div>
                                    
                                    
                                </ul>
                                
                                <!-- <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Home</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>About</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Services</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Work</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Privacy</a></li>
                                </ul> -->
                            </aside>
                        </div>
                        <div class="col-md-3">
                        <img src="<?php echo base_url(); ?>img/footer_image.png" class="footer_image" style="width=20%;" alt=""/>
                        </div>
                        <div class=" col-md-4">
                        <aside class="f_widget contact_widget">
                                <div class="f_w_title">
                                    <h3>Contact Us</h3>
                                </div>
                                <a href="#">07034367931</a>
                                <a href="#">info.techbarn@gmail.com</a>
                                <p>35 Olorun Oluwa Street <br />Ikoyi Lagos</p>
                                <h6>Open hours: 8.00-18.00 Mon-Fri</h6>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy_right_area">
                <div class="container">
                    <div class="float-md-left">
                        <h5>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://techbarn.com" target="_blank">TechBarn</a></h5>
                    </div>
                    <div class="float-md-right">
                        <ul class="nav">
                            <!-- <li class="nav-item">
                                <a class="nav-link active" href="#">Disclaimer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Privacy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Advertisement</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact us</a>
                            </li> -->
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!--================End Footer Area =================-->




        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="<?php echo base_url(); ?>vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="<?php echo base_url(); ?>vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/jquery.counterup.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/apear.js"></script>
        <script src="<?php echo base_url(); ?>vendors/counterup/countto.js"></script>
        <script src="<?php echo base_url(); ?>vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/parallaxer/jquery.parallax-1.1.3.js"></script>
        <!--Tweets-->
        <script src="<?php echo base_url(); ?>vendors/tweet/tweetie.min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/tweet/script.js"></script>

        <script src="<?php echo base_url(); ?>js/theme.js"></script>
    </body>
</html>
